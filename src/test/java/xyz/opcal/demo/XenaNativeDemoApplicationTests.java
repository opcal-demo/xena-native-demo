package xyz.opcal.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import xyz.opcal.xena.core.context.XenaContext;

@SpringBootTest
class XenaNativeDemoApplicationTests {

	@Autowired
	XenaContext xenaContext;

	@Test
	void contextLoads() {
		assertNotNull(xenaContext);
	}

}
