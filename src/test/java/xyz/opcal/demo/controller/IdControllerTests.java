package xyz.opcal.demo.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class IdControllerTests {

	@Autowired
	TestRestTemplate restTemplate;

	@Test
	void snowflake() {
		assertNotNull(restTemplate.getForObject("/snowflake", String.class));
	}

	@Test
	void uuid() {
		assertNotNull(restTemplate.getForObject("/uuid", String.class));
	}

}
