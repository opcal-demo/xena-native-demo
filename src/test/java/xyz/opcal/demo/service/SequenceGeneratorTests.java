package xyz.opcal.demo.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import xyz.opcal.demo.service.sequence.SequenceType;

@SpringBootTest
class SequenceGeneratorTests {

	@Autowired
	SequenceGenerator sequenceGenerator;

	@Test
	void test() {
		assertNotNull(sequenceGenerator.genrtateId(SequenceType.UUID));
		assertNotNull(sequenceGenerator.genrtateId(SequenceType.SNOWFLAKE));
		assertThrows(IllegalStateException.class, () -> sequenceGenerator.genrtateId(SequenceType.NOT_SUPPORT));
	}

}
