package xyz.opcal.demo.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.nativex.hint.AotProxyHint;
import org.springframework.nativex.hint.AotProxyHints;

import xyz.opcal.demo.service.SequenceGenerator;
import xyz.opcal.xena.core.annotation.EnableXena;

@ComponentScan
@Configuration
@EnableXena
@AotProxyHints(value = { @AotProxyHint(targetClass = SequenceGenerator.class) })
public class XNDemoConfiguration {

}
