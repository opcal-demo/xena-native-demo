package xyz.opcal.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.demo.service.IdService;

@RestController
public class IdController {

	@Autowired
	private IdService idService;

	@GetMapping("/snowflake")
	public String snowflake() {
		return idService.generateSnowflakeId();
	}

	@GetMapping("/uuid")
	public String uuid() {
		return idService.generateUuid();
	}

}
