package xyz.opcal.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.opcal.demo.service.sequence.IdGenerator;
import xyz.opcal.demo.service.sequence.SequenceType;
import xyz.opcal.xena.core.bean.PolymorphismBeanLoader;

@Service
public class SequenceGenerator {

	private @Autowired PolymorphismBeanLoader polymorphismBeanLoader;

	public String genrtateId(SequenceType sequenceType) {

		IdGenerator generator = polymorphismBeanLoader.getImplementsInstance(IdGenerator.class, sequenceType.getName());
		if (generator == null) {
			throw new IllegalStateException("not support sequence type");
		}
		return generator.generate();
	}
}
