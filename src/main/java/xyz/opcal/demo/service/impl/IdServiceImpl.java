package xyz.opcal.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.opcal.demo.service.IdService;
import xyz.opcal.demo.service.SequenceGenerator;
import xyz.opcal.demo.service.sequence.SequenceType;

@Service
public class IdServiceImpl implements IdService {

	@Autowired
	private SequenceGenerator sequenceGenerator;

	@Override
	public String generateSnowflakeId() {
		return sequenceGenerator.genrtateId(SequenceType.SNOWFLAKE);
	}

	@Override
	public String generateUuid() {
		return sequenceGenerator.genrtateId(SequenceType.UUID);
	}

}
