package xyz.opcal.demo.service.sequence.impl;

import javax.annotation.PostConstruct;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.demo.service.sequence.IdGenerator;
import xyz.opcal.xena.core.annotation.Polymorphism;

@Slf4j
@Polymorphism(selector = "snowflake", interfaceClass = IdGenerator.class)
public class SnowflakeGenerator implements IdGenerator {

	private Snowflake defaultSnowflake = new Snowflake(0, 0);

	@PostConstruct
	public void init() {
		log.info("SnowflakeGenerator init");
	}

	@Override
	public String generate() {
		return String.valueOf(defaultSnowflake.nextId());
	}

}
