package xyz.opcal.demo.service.sequence.impl;

import java.util.UUID;

import xyz.opcal.demo.service.sequence.IdGenerator;
import xyz.opcal.xena.core.annotation.Polymorphism;

@Polymorphism(selector = "uuid", interfaceClass = IdGenerator.class)
public class UUIDGenerator implements IdGenerator {

	@Override
	public String generate() {
		return UUID.randomUUID().toString();
	}

}
