package xyz.opcal.demo.service.sequence;

public interface IdGenerator {

	String generate();
}
