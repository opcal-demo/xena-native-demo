package xyz.opcal.demo.service.sequence;

import lombok.Getter;

public enum SequenceType {

	UUID("uuid"),
	SNOWFLAKE("snowflake"),
	NOT_SUPPORT("notSupport");
	
	
	@Getter
	private String name;
	private SequenceType(String name) {
		this.name = name;
	}
}
