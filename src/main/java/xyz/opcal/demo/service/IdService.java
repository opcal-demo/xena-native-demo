package xyz.opcal.demo.service;

public interface IdService {

	String generateSnowflakeId();

	String generateUuid();

}
